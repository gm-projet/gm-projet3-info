#include <stdio.h>
#include <stdlib.h>
#include "libdico.h"

void main() {
    // TESTS
    dictionnaire dico, dicoCopie, dico2, dicoFusion, dico3;

    // chargement
    dico = chargerDictionnaire("mon-dico.txt");
    printf("Dictionnaire chargé !\n");

    afficherDictionnaire(dico);

    // obtention de définition
    printf("Définition de stylo : %s\n", obtenirDefinition(dico, "stylo")) ;

    // changement de définition
    changerDefinition(dico, "stylo", "sert à BEAUCOUP écrire");
    printf("Nouvelle définition de stylo : %s\n", obtenirDefinition(dico, "stylo")) ;

    // suppressio, d'une entrée
    supprimerEntree(&dico, "stylo");
    sauvegarderDictionnaire(dico, "mon-dico-sauve.txt");
    printf("Dictionnaire sauvegardé !\n");

    // copie d'un dictionnaire
    dicoCopie = copierDictionnaire(dico);
    printf("Dictionnaire copié !\n");
    sauvegarderDictionnaire(dicoCopie, "mon-dico-copie-sauve.txt");
    printf("Dictionnaire copie sauvegardé !\n");

    // test fusion
    dico2 = chargerDictionnaire("mon-dico-2.txt");
    dicoFusion = fusion(dico, dico2);
    sauvegarderDictionnaire(dicoFusion, "mon-dico-fusion-sauve.txt");
    printf("Dictionnaire fusion sauvegardé !\n");
    printf("%d mots dans la fusion\n",dicoFusion.nbMots);

    //test erreur sur fichier
    printf("\nchargement d'un fichier inexistant : \n");
    dico3 = chargerDictionnaire("dico-fantôme.txt");
    printf("\nsauvegarde dans un fichier protégé : \n");
    sauvegarderDictionnaire(dico, "protected-file.txt");
    /*protected-file.txt a été protégé en écriture avec la commande chmod 555 protected-file.txt*/

    // suppressions
    supprimerDictionnaire(&dico);
    supprimerDictionnaire(&dico2);
    supprimerDictionnaire(&dicoCopie);
    supprimerDictionnaire(&dicoFusion);
    supprimerDictionnaire(&dico3);
    printf("\nNombre de mots après suppression : %d\n", dico.nbMots);

}
