#include <stdio.h>
#include "libdico.h"
#include <stdlib.h>
#include <string.h>


dictionnaire creerDico ()
{
    dictionnaire res;
    int i ;
    res.nbMots = 0;
    for (i=0 ; i<TALPHABET ; i = i + 1)
    {
        res.tabMots[i] = NULL ;
    }
    return res ;
}



entree *chercherMot (dictionnaire dico, char *mot)
{
    entree *p;
    p = dico.tabMots[mot[0]-'a'];
    while ( (p != NULL) && (strcmp(p->mot,mot) < 0) )
    {
        p = p->suiv;
    }
    if ((p != NULL) && (strcmp(p->mot,mot) == 0))
    {
        return p ;
    }
    else {return NULL ;}
}




char *obtenirDefinition (dictionnaire dico, char *mot)
{
    char *res;
    entree *p ;
    res = NULL;
    p = chercherMot (dico, mot);
    if (p != NULL)
    {
        res = malloc ( sizeof(char) * (strlen(p->definition) + 1) );
        strcpy(res, p->definition);
    }
    return res ;
}




void changerDefinition (dictionnaire dico, char *mot, char *nouvelleDef)
{
    entree *p ;
    p = chercherMot(dico,mot) ;
    if (p != NULL)
    {
        free(p->definition);
        p->definition = malloc( sizeof(char) * (strlen(nouvelleDef)+1) ) ;
        strcpy(p->definition, nouvelleDef);
    }
}

void supprimerDictionnaire (dictionnaire *dico)
{
    entree *p ;
    int i ;
    for (i = 0; i < TALPHABET; i++)
    {
        p = dico->tabMots[i] ;
        while (dico->tabMots[i] != NULL)
        {
            p = dico->tabMots[i]->suiv;
            free(dico->tabMots[i]->mot);
            free(dico->tabMots[i]->definition); //on libère les chaines de caractères de l'entrée
            free(dico->tabMots[i]);
            dico->tabMots[i] = p ;
        }
    }
    dico->nbMots = 0;
}

void sauvegarderDictionnaire (dictionnaire dico, char *nomFichier)
{
    entree *p ;
    int i ;
    FILE *fic ;
    fic = fopen(nomFichier,"w");
    if (fic != NULL)
    {
        for (i = 0; i < TALPHABET; i++)
        {
            p = dico.tabMots[i];
            while(p != NULL)
            {
                fprintf(fic, "%s \"%s\"\n", p->mot,p->definition);
                p = p->suiv ;
            }
        }
        fclose(fic);
    }
    else {printf("\nsauvegarderDictionnaire : erreur lors de la création du fichier\n");}
}




void inserer (char *mot, char *definition, dictionnaire *dico)
{
    entree *p, *q, *listeMots;

    q = malloc(sizeof(entree));
    q->mot = mot;
    q->definition = definition;
    q->suiv = NULL;
    listeMots = dico->tabMots[mot[0] - 'a'];
    p = listeMots;
    if (listeMots == NULL)
    {
        dico->tabMots[mot[0] - 'a'] = q;
    }
    else
    {
        while ((p->suiv != NULL) && (strcmp(p->suiv->mot, mot) <= 0))
        {
            p = p->suiv;
        }
        if (p == NULL)
        {
            p = q;
        }
        else
        {
            q->suiv = p->suiv;
            p->suiv = q;
        }
    }
    dico->nbMots++;
}




void supprimerEntree (dictionnaire *dico, char *mot)
{
    entree *listeMots;

    listeMots = dico->tabMots[mot[0] - 'a'];
    if (listeMots != NULL)
    {
        if ((listeMots->suiv == NULL) && (strcmp(listeMots->mot, mot) == 0))
        {
            free(dico->tabMots[mot[0] - 'a']->mot);
            free(dico->tabMots[mot[0] - 'a']->definition);
            free(dico->tabMots[mot[0] - 'a']);
            dico->tabMots[mot[0] - 'a'] = NULL;
        }
        else
        {
            entree *p = listeMots, *q = listeMots->suiv;

            while ((q->suiv != NULL) && (strcmp(q->mot, mot)))
            {
                q = q->suiv;
                p = p->suiv;
            }
            p->suiv = q->suiv;
            free(q->mot);
            free(q->definition);
            free(q);
        }
        dico->nbMots--;
    }
}




dictionnaire chargerDictionnaire (char* nomFichier)
{
    FILE *fic;
    char *mot, *definition;
    char tabMot[TMOT], tabDefinition[TDEFINITION];
    dictionnaire dico;

    dico = creerDico();
    fic = fopen(nomFichier, "r");
    if (fic != NULL)
    {
        while (fscanf(fic, "%s", tabMot), !feof(fic))
        {
            unsigned int j;
            fgets(tabDefinition, TDEFINITION, fic);
            // on enlève les guillemets du fichier
            tabDefinition[strlen(tabDefinition) - 2] = '\0';
            for (j = 0; j < strlen(tabDefinition); j++)
            {
                tabDefinition[j] = tabDefinition[j + 2];
            }
            // on alloue les chaînes dynamiques
            mot = malloc((strlen(tabMot) + 1) * sizeof(char));
            strcpy(mot, tabMot);
            definition = malloc((strlen(tabDefinition) + 1) * sizeof(char));
            strcpy(definition, tabDefinition);
            inserer(mot, definition, &dico);
        }
        fclose(fic);
    }
    else {printf("\nchargerDictionnaire : erreur lors de l'ouverture du fichier\n");}
    return dico;
}




dictionnaire copierDictionnaire (dictionnaire src)
{
    dictionnaire res;
    int i ;
    char* tempstr ;
    entree* psrc ;
    entree* pres ;
    res = creerDico() ;
    for (i = 0; i < TALPHABET; i++)
    {
        psrc = src.tabMots[i] ;
        pres = NULL ;
        while (psrc != NULL)
        {
            if (pres != NULL)
            {
                pres->suiv = malloc(sizeof(entree));
                tempstr = malloc(sizeof(char)*(1+strlen(psrc->mot))) ;
                pres->suiv->mot = strcpy(tempstr,psrc->mot) ;
                tempstr = malloc(sizeof(char)*(1+strlen(psrc->definition))) ;
                pres->suiv->definition = strcpy(tempstr,psrc->definition) ;
                pres->suiv->suiv = NULL ;
                pres = pres->suiv ;
            }
            else
            {
                pres = malloc(sizeof(entree));
                tempstr = malloc(sizeof(char)*(1+strlen(psrc->mot))) ;
                pres->mot = strcpy(tempstr,psrc->mot) ;
                tempstr = malloc(sizeof(char)*(1+strlen(psrc->definition))) ;
                pres->definition = strcpy(tempstr,psrc->definition) ;
                pres->suiv = NULL ;
                res.tabMots[i] = pres ;
            }
            psrc = psrc->suiv;
        }
    }
    res.nbMots = src.nbMots ;
    return res;
}


dictionnaire fusion (dictionnaire dico1, dictionnaire dico2)
{
    int i ;
    dictionnaire dico3;
    entree *p1, *p2, *p3 ;
    char *tempstr ;

    dico3 = creerDico() ;
    for (i = 0; i < TALPHABET; i++)
    {
        p1 = dico1.tabMots[i];
        p2 = dico2.tabMots[i];
        while ( (p1 != NULL) || (p2 != NULL) )
        {
            if (dico3.tabMots[i] == NULL)
            {
                dico3.tabMots[i] = malloc(sizeof(entree)) ;
                p3 = dico3.tabMots[i] ;
            }
            else
            {
                p3->suiv = malloc(sizeof(entree)) ;
                p3 = p3->suiv ;
            }
            if ((p1 == NULL) || ( (p2 != NULL) && (strcmp(p1->mot, p2->mot) > 0) ) )
            {
                tempstr = malloc(sizeof(char)*(1+strlen(p2->mot))) ;
                p3->mot = strcpy(tempstr,p2->mot) ;
                tempstr = malloc(sizeof(char)*(1+strlen(p2->definition))) ;
                p3->definition = strcpy(tempstr,p2->definition) ;
                p3->suiv = NULL ;
                p2 = p2->suiv ;
                dico3.nbMots = dico3.nbMots + 1 ;
            }
            else
            {
                tempstr = malloc(sizeof(char)*(1+strlen(p1->mot))) ;
                p3->mot = strcpy(tempstr,p1->mot) ;
                tempstr = malloc(sizeof(char)*(1+strlen(p1->definition))) ;
                p3->definition = strcpy(tempstr,p1->definition) ;
                p3->suiv = NULL ;
                if ( (p2 != NULL) && (strcmp(p1->mot,p2->mot) == 0) )
                {
                    p2 = p2->suiv ;
                }
                p1 = p1->suiv ;
                dico3.nbMots = dico3.nbMots + 1 ;
            }
        }
    }
    return dico3;
}




void afficherDictionnaire (dictionnaire dico)
{
    int i;
    printf("\nNombre de mots : %d\n",dico.nbMots);
    for (i = 0; i < TALPHABET; i++)
    {
        entree *p = dico.tabMots[i];
        if (p != NULL)
        {
            printf("\n%c\n", i + 'A');
            printf("______________________________________\n");
            while (p != NULL)
            {
                printf("- %s : %s\n", p->mot, p->definition);
                p = p->suiv;
            }
        }
    }
    printf("\n");
}
