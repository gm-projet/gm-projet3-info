#ifndef __LIBDICO_H__
#define __LIBDICO_H__
#define TALPHABET 26
#define TMOT 30
#define TDEFINITION 200

typedef struct structEntree {
    char *mot ;
    char *definition ;
    struct structEntree *suiv;
} entree;

typedef struct structDico {
    entree *tabMots[TALPHABET];
    unsigned int nbMots ;
} dictionnaire;

dictionnaire creerDico ();//renvoi un dictionnaire vide

entree *chercherMot (dictionnaire dico, char *mot); //renvoi un pointeur sur l'entrée du mot cherché

char *obtenirDefinition (dictionnaire dico, char *mot); //retourne NULL si le mot n'est pas dans le dico

void changerDefinition (dictionnaire dico, char *mot, char *nouvelleDef); // si le mot n'est pas dans le dico ne fait rien

void inserer (char *mot, char *definition, dictionnaire *dico); // insère un mot dans le dictionnaire en respectant l'ordre alphabétique

void supprimerEntree (dictionnaire *dico, char *mot); // supprime un mot du dictionnaire

dictionnaire chargerDictionnaire (char* nomFichier); // charge un dictionnaire à partir d'un fichier

void supprimerDictionnaire (dictionnaire *dico); //vide le dictionnaire

void sauvegarderDictionnaire (dictionnaire dico, char *nomFichier); //sauvegarde le dictionnaire dans un fichier

dictionnaire copierDictionnaire (dictionnaire src) ; //renvoi un dictionnaire avec le même contenu que celui donné en entrée

dictionnaire fusion (dictionnaire dico1, dictionnaire dico2) ; //renvoi un dictionnaire contenant les mots de dico1 et ceux de dico2
/*si un mots apparait dans les 2 seule la définition du dico1 sera retenue*/

void afficherDictionnaire (dictionnaire dico); // affiche un dictionnaire dans le terminal


#endif
