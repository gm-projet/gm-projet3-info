\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[colorlinks = true,
			linkcolor = blue]{hyperref}
\usepackage{graphicx}
%opening
\title{Projet d'informatique : gestion de dictionnaires}
\author{Robin \textsc{Langlois} et Nathan \textsc{Flambard}}

\begin{document}

\maketitle

\begin{abstract}
Projet Info-08 : le but est de proposer une structure de données permettant de gérer un dictionnaire électronique. 
\end{abstract}

\newpage

\tableofcontents

\newpage

\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}

On veut pouvoir créer un dictionnaire recueillant des mots ainsi que leur définition. On définit alors le TAD Dictionnaire de la façon suivante :\\

\underline{Sorte :} Dictionnaire

\underline{Utilise :} Chaîne

\underline{Opérations :}

créerDico() $\rightarrow$ Dictionnaire

insérer(Dictionnaire, Chaîne, Chaîne) $\rightarrow$ Dictionnaire

obtenirDéfinition(Dictionnaire, Chaîne) $\rightarrow$ Chaîne

supprimerEntrée(Dictionnaire, Chaîne) $\rightarrow$ Dictionnaire

changerDéfinition(Dictionnaire, Chaîne, Chaîne) $\rightarrow$ Dictionnaire

chargerDictionnaire(Chaîne) $\rightarrow$ Dictionnaire

sauvegarderDictionnaire(Dictionnaire, Chaîne)

supprimerDictionnaire(Dictionnaire) $\rightarrow$ Dictionnaire

copierDictionnaire(Dictionnaire) $\rightarrow$ Dictionnaire \\

On choisit donc la structure de données suivante :

\begin{verbatim}
Type 
    Dictionnaire = Enregistrement
        tabMots : Tableau[1..26] de ^Entree
        nbMots : Entier
    finenregistrement
    
    Entree = Enregistrement
        mot, definition : Chaîne
        suiv : Dictionnaire
    finenregistrement
\end{verbatim}

Notre dictionnaire est donc un tableau de listes chaînées d'entrées, une entrée étant un mot et une définition. Chaque case du tableau correspond à une lettre de l'alphabet, qui est donc associée à la liste des mots commençant par cette lettre. L'avantage de cette structure, hybride entre tableau et simple liste chaînée, permet d'associer les avantages de ces deux derniers : on dispose d'un accès direct à chaque lettre de l'alphabet, ce qui est pratique, et on peut ajouter autant de mots qu'on veut pour chaque lettre sans vraiment de limite. On ajoute également le nombre de mots pour pouvoir y accéder rapidement sans devoir parcourir toutes les listes du tableau. On mettra bien évidemment à jour ce nombre à chaque ajout ou suppression d'entrée. On choisira comme convention que tous les mots sont écrits entièrement en minuscules.

Pour le besoin des opérations du type abstrait de données défini ci-dessus, on ajoute la fonction suivante associée à notre implémentation : \verb!chercherMot!. Elle permet de chercher dans le dictionnaire si un mot est présent, et renvoie un pointeur sur l'entrée correspondante si c'est le cas. Sinon, on renvoie \verb!nil!. Cette fonction ne sera pas utile à proprement parler pour l'utilisateur mais nous servira lors de l'implémentation des opérations du TAD.\\

chercherMot(Dictionnaire, Chaîne) $\rightarrow$ $\uparrow$Entree




\section{Conception détaillée}

\subsection{Opérations du TAD}

\subsubsection*{créerDico}
créerDico() $\rightarrow$ Dictionnaire\\

Cette fonction permet d'initialiser un dictionnaire en tant que dictionnaire ne contenant aucun mot. Pour cela il suffit de renvoyer un dictionnaire dont le champ \verb!nbMots! vaut $0$ et dont tous les éléments du champ \verb!tabMots! sont à \verb!nil!.

\begin{verbatim}
fonction créerDico() : Dictionnaire
déclaration res : Dictionnaire
            i : Entier
début
    res.nbMots <- 0
    pour i <- 1 à 26 inc +1 faire
        res.tabMots[i] <- nil
    finpour
    retourner res
fin
\end{verbatim}



\subsubsection*{insérer}
insérer(Dictionnaire, Chaîne, Chaîne) $\rightarrow$ Dictionnaire\\

Cette procédure permet d'insérer une nouvelle entrée dans le dictionnaire. On dispose du dictionnaire et du mot ainsi que de la définition de l'entrée à insérer, et on doit l'ajouter au dictionnaire en respectant l'ordre alphabétique.

On doit d'abord une nouvelle case mémoire pour la nouvelle entrée, à laquelle on attribuera le mot et la définition voulue. On récupère alors la première lettre du mot pour savoir sur quelle case du tableau on doit travailler. On doit ensuite distinguer plusieurs cas de figures :
- si la liste est vide
- si la liste est non vide, et qu'on insère en début ou en milieu de liste
- si la liste est non vide, et qu'on insère en fin de liste

Si la liste est initialement vide, alors on dit que cette liste est maintenant le pointeur qui pointe sur l'entrée que nous avons créé. Sinon, on parcourt la liste jusqu'à ce qu'on trouve le premier mot qui se situe lexicographiquement avant le mot que nous voulons insérer. Une fois qu'on l'a trouvé, on insère notre entrée juste avant le mot trouvé : on fait pointer notre entrée sur le mot trouvé, puis on fait pointer le mot précédent sur notre entrée. Si on est arrivés en fin de liste, on fait simplement pointer la fin de la liste sur notre pointeur.

Voici le pseudo-code :
\begin{verbatim}
procédure inserer(E mot, definition : Chaîne ; E/S dico : Dictionnaire)
déclaration p, q, listeMots : ^Entree
début
    q <- allouer(Entree)
    q^.mot <- mot
    q^.definition <- definition
    q^.suiv <- nil
    listeMots <- dico.tabMots[ord(ieme(mot, 1)) - ord('a') + 1]
    p <- listeMots
    si listeMots = nil alors
        dico.tabMots[ord(ieme(mot, 1)) - ord('a') + 1] <- q
    sinon
        tant que (ord(p^.suiv^.mot)) <= ord(mot)) et (p != nil) faire
            p <- p^.suiv
        fintantque
        si p = nil alors
            p <- q
        sinon
            q^.suiv <- p^.suiv
            p^.suiv <- q
        finsi
    finsi
    dico.nbMots <- dico.nbMots + 1
fin
\end{verbatim}


\subsubsection*{obtenirDéfinition}
obtenirDéfinition(Dictionnaire, Chaîne) $\rightarrow$ Chaîne\\

Cette fonction renvoie la définition d'un mot dans une chaine de caractères. On a en entrée le dictionnaire dans lequel on cherche la définition et le mot dont on veut connaître le sens. Pour obtenir notre résultat, il suffit d'utiliser la fonction \verb!chercherMot!. Si le mot est dans le dictionnaire, il suffit de retourner le champ définition du pointeur renvoyé par \verb!chercherMot!. Sinon on renvoie simplement une chaine de caractère vide.

\begin{verbatim}
fonction obtenirDéfinition (dico : Dictionnaire ; mot : Chaine) : Chaine
déclaration p : ^Entrée
            res : Chaine
début
    res <- ""
    p <- chercherMot (dico, mot)
    si (p != nil) alors
        res <- p^.définition
    finsi
    retourner res
fin
\end{verbatim}

\subsubsection*{supprimerEntrée}
supprimerEntrée(Dictionnaire, Chaîne) $\rightarrow$ Dictionnaire\\

Cette procédure supprime une entrée dans le dictionnaire donné à partir du mot associé, si celui-ci est présent. Si la liste est vide, on ne fait rien. Sinon, on regarde si la liste est composée d'une seule entrée. Si c'est le cas, on supprime cette unique entrée si elle correspond bien au mot voulu, sinon on ne fait rien. Si la liste est composée de plusieurs éléments, on la parcourt avec deux pointeurs jusqu'à trouver le mot voulu (on aura besoin d'un deuxième pointeur pour garder l'endroit où nous sommes après avoir supprimé l'entrée voulue). Une fois que l'on a trouvé le mot voulu, on prend le pointeur de l'élément précédent, et on le fait pointer sur l'élément suivant le mot trouvé (on court-circuite en quelque sorte le mot à supprimer), puis on libère l'élément à supprimer.

%TODO DESSIN !!!

Cela donne l'algorithme suivant :

\begin{verbatim}
procédure supprimerEntree(E/S dico : Dictionnaire ; E mot : Chaîne)
déclaration p, q, listeMots : ^Entree
début
    listeMots <- dico[ord(ieme(mot, 1)) - ord('a') + 1]
    si listeMots != nil alors
        si (listeMots^.suiv = nil) et (listeMots^.mot = mot) alors
            récupérer(listeMots)
            dico[ord(ieme(mot, 1)) - ord('a') + 1] <- nil
        sinon
            p <- listeMots
            q <- listeMots^.suiv
            tant que (q^.mot != mot) et (q^.suiv != nil) alors
                q <- q^.suiv
                p <- p^.suiv
            fintantque
            p^.suiv <- q^.suiv
            récupérer(q)
        finsi
        nbMots <- nbMots - 1
    finsi
fin
\end{verbatim}


\subsubsection*{changerDéfinition}
changerDéfinition(Dictionnaire, Chaîne, Chaîne) $\rightarrow$ Dictionnaire\\

Cette procédure remplace la définition d'un mot présent dans le dictionnaire par une définition donnée en entrée. Pour faire cela, on utilise la fonction \verb!chercherMot! pour obtenir un pointeur sur l'entrée correspondant au mot donné en entrée. Si le mot est dans le dictionnaire, on remplace le champ \verb!définition! par la nouvelle définition donnée en entrée. Si le mot n'est pas dans le dictionnaire, on ne fait rien.

\begin{verbatim}
procédure changerDéfinition (E/S dico : Dictionnaire ; E mot, nouvelleDef : Chaine)
déclaration p : ^Entrée
début
    p <- chercherMot (dico, mot)
    si (p != nil) alors
        p^.définition <- nouvelleDef
    finsi
fin

\end{verbatim}
\subsubsection*{chargerDictionnaire}
chargerDictionnaire(Chaîne) $\rightarrow$ Dictionnaire\\

Cette procédure permettra de charger un dictionnaire à partir d'un fichier. Dans ce fichier, les mots ne seront pas forcément dans l'ordre alphabétique. On prendra la convention suivante pour les fichiers : \\

mot1 ``définition1''

mot2 ``définition2''

...\\

La procédure prend en paramètre le nom du fichier à charger, et donne en sortie un dictionnaire. Il est à noter que nous utilisons dans l'algorithme la procédure \verb!donneMot! vue en TD. Lors de notre implémentation en C, nous n'aurons pas besoin de cette procédure grâce au formatage des chaînes de caractère que le langage permet.
Pour résumer, nous lisons simplement le fichier ligne par ligne, on récupère le mot, puis la définition. On enlève les guillemets de la définition qui étaient dans le fichier, puis on utilise la fonction \verb!inserer! (qui s'occupera de placer le mot au bon endroit en respectant l'ordre alphabétique, et qui incrémentera le nombre de mots du dictionnaire). On n'oublie pas de fermer le fichier !

\begin{verbatim}
procédure chargerDictionnaire(E nomFichier : Chaîne ; S dico : Dictionnaire)
déclaration fichier : FT
            ligne, mot, definition : Chaîne
            i : Entier
début
    fichier <- ouvrirEnLecture(nomFichier)
    dico <- creerDico()
    tant que non finDeFichier(fichier) faire
        i <- 0
        lireChaine(fichier, ligne)
        donneMot(ligne, i, mot)
        definition <- copier(ligne, lg(mot) + 2, lg(ligne) - (lg(mot) + 1))
        effacer(definition, 1, 1)
        effacer(definition, lg(definition), 1)
        inserer(mot, definition, dico)
    fintantque
    fermer(fichier)
fin
\end{verbatim}


\subsubsection*{sauvegarderDictionnaire}
sauvegarderDictionnaire(Dictionnaire, Chaîne)\\

Cette procédure permet d'écrire un dictionnaire dans un fichier texte. On rangera les mots dans l'ordre alphabétique. On prendra la même convention  de fichier que pour \verb!chargerDictionnaire! :\\

mot1 ``définition1''

mot2 ``définition2''

...\\
On prend en entrée le dictionnaire à stocker ainsi que le nom du fichier dans lequel on le stocke. Pour commencer, on crée un fichier avec le nom donné en entrée (si il existe déjà, on le remplace). Puis pour chaque cellule du tableau, on parcourt la liste d'entrée et on écrit dans le fichier les mots et définitions selon la convention choisie. Comme les listes du dictionnaires sont ordonnées dans l'ordre alphabétique, le fichier le sera aussi. Enfin on ferme le fichier.
\begin{verbatim}
procédure sauvegarderDictionnaire(E dico : Dictionnaire ; nomFichier : Chaine)
déclaration p : ^Entrée
            fic : FT    //type fichier texte
            i : Entier
début
    fic <- créerFichier(nomFichier)
    pour i <- 1 à 26 inc +1 faire
        p <- dico.tabMots[i]
        tant que (p != nil) faire
            écrire(fic, p^.mot)
            écrire(fic," \"")   //où \" est le caractère "
            écrire(fic, p^.définition)
            écrire(fic, ""\n")  //où \n est le caractère de saut de ligne
            p <- p^.suiv
        fintantque
    finpour
    fermer(fic)
fin
\end{verbatim}

\subsubsection*{supprimerDictionnaire}
supprimerDictionnaire(Dictionnaire) $\rightarrow$ Dictionnaire\\

Cette procédure vide un dictionnaire de tous ses mots. Pour ce faire, on vide toutes les listes du champ \verb!tabMots! et on passe le champ \verb!nbMots! à $0$. Pour vider une liste, il suffit de supprimer l'entrée en tête de liste jusqu'à ce que la liste soit vide. 
\begin{verbatim}
procédure supprimerDictionnaire (E/S dico : Dictionnaire)
déclaration p : ^Entrée
            i : Entier
début
    pour i <- 1 à 26 inc +1 faire
        p <- dico.tabMots[i]
        tant que (dico.tabMots[i] != nil) faire
            p <- dico.tabMots[i]^.suiv
            récupérer(dico.tabMots[i])
            dico.tabMots[i] <- p
        fintantque
    finpour
    dico.nbMots = 0
fin
\end{verbatim}

\subsubsection*{copierDictionnaire}
copierDictionnaire(Dictionnaire) $\rightarrow$ Dictionnaire \\

Cette fonction renvoie un nouveau dictionnaire contenant les mêmes mots et les mêmes définitions que celui fourni en entrée. Pour faire cela, on recopie chaque liste du champ \verb!tabMots! élément par élément dans un nouveau dictionnaire. Pour cela on utilise un pointeur sur la prochaine \verb!Entrée! de la liste du dictionnaire donné en entrée à recopier (\verb!psrc!) et un pointeur sur la dernière \verb!Entrée! recopiée dans le dictionnaire de sortie (\verb!pres!). Pour rajouter l'\verb!Entrée! pointée par \verb!psrc! dans le dictionnaire de sortie, il suffit de recopier ses champs \verb!mot! et \verb!définition! dans une \verb!Entrée! et de mettre l'adresse de cette dernière dans le champ \verb!suiv! de l'\verb!Entrée! pointée par \verb!pres!. Pour le premier élément de la liste (quand \verb!pres = nil!), on met l'adresse de la nouvelle adresse dans \verb!pres! et dans le champ \verb!tabMots! du dico de sortie pour garder un pointeur sur le début de la liste.    

\begin{verbatim}
fonction copierDictionnaire(src : Dictionnaire) : Dictionnaire
déclaration res : Dictionnaire
            i : Entier
            psrc, pres : ^Entrée
début
    res <- créerDico()
    pour i <- 1 à 26 inc +1 faire
        psrc <- src.tabMots[i]
        pres <- nil
        tant que (psrc != nil) faire
            si (pres != nil) alors
                pres^.suiv <- allouer(Entrée)
                pres^.suiv^.mot <- psrc^.mot
                pres^.suiv^.définition <- psrc^.définition
                pres^.suiv^.suiv <- nil
                pres <- pres^.suiv
            sinon
                pres <- allouer(Entrée)
                pres^.mot <- psrc^.mot
                pres^.définition <- psrc^.définition
                pres^.suiv <- nil
                res.tabMots[i] <- pres
            finsi
        fintantque
    finpour
    res.nbMots <- src.nbMots
    retourner res
fin
\end{verbatim}




\subsubsection*{chercherMot}
chercherMot(Dictionnaire, Chaîne) $\rightarrow$ $\uparrow$Entree\\

Cette fonction cherche un mot dans un dictionnaire et renvoie un pointeur sur l'entrée correspondante. Si le mot n'est pas dans le dictionnaire, cette fonction renvoie la valeur \verb!nil!. Pour trouver un mot dans notre dictionnaire, on place tout d'abord un pointeur (\verb!p! dans l'algorithme) dans la liste correspondante à la première lettre du mot cherché. Puis on utilise ce pointeur pour parcourir la liste jusqu'à trouver le premier mot qui ne précède pas le mot cherché dans l'ordre alphabétique (on s'arrête soit quand on arrive en fin de liste donc quand \verb!p = nil! soit quand \verb!ord(p^.mot) >= ord(mot)!). Ensuite il y a deux cas possibles : soit notre pointeur contient l'adresse de l'\verb!Entrée! recherchée (\verb!p^.mot = mot!) et on le renvoie (il faut bien sûr d'abord vérifier que \verb!p! contient une adresse différente de \verb!nil!), soit le mot n'est pas dans le dictionnaire donc on renvoie \verb!nil!.
\begin{verbatim}
fonction chercherMot (dico : Dictionnaire ; mot : Chaine) : ^Entrée
déclaration p : ^Entrée
début
    p <- dico.tabMots[ord(ième(mot,1)) - ord('a') + 1]
    tant que (p != nil) et (ord(p^.mot) < ord(mot)) faire
        p <- p^.suiv
    fintantque
    si (p != nil) et (mot = p^.mot) alors
        retourner p
    sinon
        retourner nil
    finsi
fin
\end{verbatim}


\subsection{Fusion}
On s'intéresse maintenant à l'algorithme de la fonction \verb!fusion! :\\
fusion(Dictionnaire, Dictionnaire) $\rightarrow$ Dictionnaire\\

Cette fonction prend en entrée deux dictionnaires (\verb!dico1! et \verb!dico2!) et renvoie un troisième dictionnaire (\verb!dico3!) qui contient tout les mots et définitions de \verb!dico1! et de \verb!dico2!. On prendra comme convention que si un mot apparaît dans les deux dictionnaires, on fait seulement apparaître la définition de \verb!dico1! dans le dictionnaire de sortie. Fusionner les deux dictionnaires revient à fusionner les listes des champs \verb!tabMots! de \verb!dico1! et \verb!dico2!. Pour fusionner, on utilise le même principe que dans l'algorithme du tri fusion. On commence par mettre deux pointeurs (\verb!p1! et \verb!p2!) en tête des listes à fusionner. Puis on compare les deux \verb!Entrée! et on insère celle dont le champ \verb!mot! arrive en premier dans l'ordre lexicographique à la fin de la liste de \verb!dico3!. On répète cette opération jusqu'à ce qu'on soit arrivés à la fin d'une des deux listes et on recopie toute les \verb!Entrée! qu'il reste à parcourir de l'autre dans le \verb!dico3!. A chaque fois que l'on rajoute un mot dans \verb!dico3!, on incrémente son nombre de mots. Lors de la première insertion, on n'oublie pas de récupérer l'adresse de la tête de liste dans le champ \verb!tabMots! de \verb!dico3!.    

\begin{verbatim}
fonction fusion(dico1,dico2 : Dictionnaire) : Dictionnaire
déclaration i : Entier
            dico3 : Dictionnaire
            p1, p2, p3 : ^Entrée
début
    dico3 <- créerDico
    pour i <- 1 à 26 inc +1 faire
        p1 <- dico1.tabMots[i]
        p2 <- dico2.tabMots[i]
        tant que (p1 != nil) ou (p2 != nil) faire
            
            si (dico3.tabMots[i] = nil) alors
                dico3.tabMots[i] <- allouer(Entrée)
                p3 <- dico3.tabMots[i]
            sinon
                p3.suiv <- allouer(Entrée)
                p3 <- p3.suiv
            finsi
            
            si (p1 = nil) ou ( (p2 != nil) et (ord(p1^.mot) > ord(p2^.mot) )) alors
                p3^.mot <- p2^.mot
                p3^.définition <- p2^.définition
                p3^.suiv <- nil
                p2 <- p2^.suiv
                dico3.nbMots <- dico3.nbMots + 1
            sinon
                p3^.mot <- p1^.mot
                p3^.définition <- p1^.définition
                p3^.suiv <- nil
                si (p2 != nil) et (p1^.mot = p2^.mot)
                    p2 <- p2^.suiv
                finsi
                p1 <- p1^.suiv
                dico3.nbMots <- dico3.nbMots + 1
            finsi
        fintantque
    finpour
    retourner dico3
fin
\end{verbatim}
\section{Exemple d'exécution}
Nous avons implémenté nos algorithmes en C et écrit un programme pour tester nos codes. Ce programme charge un dictionnaire, l'affiche, récupère la définition d'un mot, modifie une définition, supprime une entrée, copie un dictionnaire et fait une fusion. On teste également les erreurs d'ouverture de fichier dans les procédures de chargement et de sauvegarde. Enfin, on supprime les dictionnaires que nous avons utilisé. Voici un exemple d'exécution : \\
\includegraphics[scale=0.5]{images/execution1.png}\\

Le dictionnaire chargé est le suivant :

\begin{verbatim}
banane "fruit jaune"
voiture "vroum vroum"
stylo "sert à écrire"
locomotive "tchou tchou"
arbre "truc vert"
bateau "sur l'eau"
\end{verbatim}

On le fusionne ensuite avec le dictionnaire suivant :
\begin{verbatim}
pression "comme la bière"
colère "pas content"
jaune "contraire de pas jaune"
banane "fruit jaune"
pâtes "c'est vraiment trop bon"
locomotive "tchou tchou"
\end{verbatim}

Et cela nous donne le résultat :

\begin{verbatim}
arbre "truc vert"
banane "fruit jaune"
bateau "sur l'eau"
colère "pas content"
jaune "contraire de pas jaune"
locomotive "tchou tchou"
pression "comme la bière"
pâtes "c'est vraiment trop bon"
voiture "vroum vroum"
\end{verbatim}

On peut voir que les dictionnaires ont bien été fusionnés : les mots en double n'apparaissent qu'une fois et tous les mots des deux dictionnaires apparaissent bien dans l'ordre alphabétique. On peut remarquer que ``pâtes'', qui contient un â, apparaît donc après ``pression'' car le â est  après le r dans l'ordre alphabétique utilisé par strcmp (UTF-8).

\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}
Ce projet nous a permis d'approfondir nos connaissances sur les listes chaînées et d'en voir une application concrète. Nous avons dû choisir une structure de données adaptée à notre problème, définir les opérations d'un TAD, et écrire les algorithmes correspondants. En plus de nous exercer sur l'algorithmique, nous avons donc pu voir le passage à l'implémentation en C. Cela nous a posé plusieurs problèmes, notamment la lecture du fichier et son formatage ou la gestion des pointeurs dans les procédures et fonctions. Néanmoins, certains aspects nous ont aidé, notamment ce dit formatage auquel il a fallu s'adapter mais qui nous a grandement facilité la tâche, ou encore les opérations immédiates sur les chaînes de caractère.

\end{document}
